<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prizes extends Model
{
    public $incrementing = true;
    public $timestamps = true;
    protected $table = 'prizes';

    protected $fillable = [
        'name', 'cost', 'count'
    ];

    public function links()
    {
        return $this->hasOne('App\Links');
    }

    public static function addPrize($prize)
    {
        return self::create($prize);
    }

}
