<?php

namespace App\Imports;

use App\Participants;
use Maatwebsite\Excel\Concerns\ToModel;

class ParticipantsImport implements ToModel
{
    /**
     * @inheritDoc
     */
    public function model(array $row)
    {
        return new Participants([
            'mobile_number' => $row[0],
        ]);
    }
}
