<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrizePool extends Model
{
    public $incrementing = true;
    public $timestamps = true;
    protected $table = 'prize_pool';

    protected $fillable = [
        'prize_pool'
    ];

}
