<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;
class AuthenticateAdmin
{
    public function handle($request, Closure $next)
    {

        if (! Auth::guard('admin')->user()) {

            return redirect('/login_form');
        }

        return $next($request);
    }
}
