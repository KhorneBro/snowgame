<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showForm(Request $request)
    {
        $urlPrevious = url()->previous();
        $urlBase = url()->to('/');
        if (($urlPrevious != $urlBase . '/login_form') && (substr($urlPrevious, 0, strlen($urlBase)) === $urlBase)) {
            session()->put('url.intended', $urlPrevious);
        }
        return view('admin.login');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $guard = 'admin';


    /**
     *
     *
     * @param Request $request
     * @return View admin.login
     */
    protected function authenticated(Request $request)
    {
        $validation = $request->validate([
            $this->username() => 'required',
            'password' => 'required',
        ],
            [
                'password.required' => 'Не верный пароль',
            ]);
        if (Auth::guard('admin')->attempt(
            [
                'email' => $request->email,
                'password' => $request->password
            ],
            $request->remember)) {
            return redirect()->intended();
        }

        return redirect()->back()->withErrors($validation)->withInput($request->only('login', 'remember'));
    }


    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Logout from admin panel
     *
     * @return Redirect admin.login
     */
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/login_form');
    }

    /**
     * Create new application guard
     *
     * @return Auth new guard as admin
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }
}
