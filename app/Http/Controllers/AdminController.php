<?php

namespace App\Http\Controllers;

use App\Exports\ParticipantsExports;
use App\Imports\ParticipantsImport;
use App\Links;
use App\Participants;
use App\PrizePool;
use App\Prizes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{

    /**
     * вьюха для админов
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    /*************************************************************************
     * ************************** администраторы *****************************
     *************************************************************************/

    /**
     * вьюха для работы с админами
     */
    public function administrators()
    {

        $page_name = 'Администраторы';
        $unit_name = 'Список администраторов';

        $admins = User::get();
        return view('admin.administrators',
            [
                'administrators' => $admins,
                'page_name' => $page_name,
                'unit_name' => $unit_name,
            ]);
    }

    /**
     * добавление админов
     */
    public function addAdmin(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|email|unique:users|regex:([\w.-]+@([\w-]+)\.+\w{2,})',
            'password' => 'required|string|max:255',
        ],
            [
                'email.email' => ':attribute заполнен не верно',
                'required' => 'поле :attribute не заполнено',
                'max' => 'поле :attribute не должно превышать длинну 255 символов',
                'min' => 'поле :attribute не может быть меньше нуля'
            ]);
        if ($validation->fails()) {

            $response = ['success' => false, 'errors' => $validation->getMessageBag()->toArray()];
            return response()->json($response, 422);

        } else {
            $admin = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
            ];
            User::addAdmin($admin);
        }
        return response()->json(['success' => true], 200);
    }

    /**
     * обновление админов
     */
    public function updateAdmin(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255|email|regex:([\w.-]+@([\w-]+)\.+\w{2,})',
            'password' => 'nullable|string|max:255',
        ],
            [
                'email.email' => ':attribute заполнен не верно',
                'name' => 'поле "Имя" не заполнено',
                'email' => 'поле "email" не заполнено',
                'max' => 'поле :attribute не должно превышать длинну 255 символов',
                'min' => 'поле :attribute не может быть меньше нуля'

            ]);
        if ($validation->fails()) {

            $response = ['success' => false, 'errors' => $validation->getMessageBag()->toArray()];
            return response()->json($response, 422);

        } else {

            $admin = User::find($id);
            $admin->name = $request->input('name');
            $admin->email = $request->input('email');

            if ($request->input('password')) {
                $admin['password'] = Hash::make($request->input('password'));
            }

            if ($admin->save()) {
                return response()->json(['success' => true], 200);
            } else {
                return response()->json(['success' => false], 422);
            }
        }
    }

    /**
     * редактирование админов
     */
    public function editAdmin(Request $request, $id)
    {
        return User::where('id', $id)->first();
    }

    /**
     * удаление админов
     */
    public function deleteAdmin(Request $request, $id)
    {
        return User::delete();
    }

    /*************************************************************************
     * ************************** Призы *****************************
     *************************************************************************/

    /**
     * вьюха призов
     */
    public function prizes()
    {
        $page_name = 'Призы';
        $unit_name = 'Список призов';

        $prizes = Prizes::get();
        return view('admin.prizes',
            [
                'page_name' => $page_name,
                'unit_name' => $unit_name,
                'prizes' => $prizes,
            ]);
    }

    /**
     * добавление призов
     */
    public function addPrize(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'cost' => 'required|integer',
            'count' => 'required|integer',
        ],
            [
                'required' => 'поле :attribute не заполнено',
            ]);
        if ($validation->fails()) {

            $response = ['success' => false, 'errors' => $validation->getMessageBag()->toArray()];
            return response()->json($response, 422);

        } else {
            $admin = [
                'name' => $request->input('name'),
                'cost' => $request->input('cost'),
                'count' => $request->input('count'),
            ];
            Prizes::addPrize($admin);
        }
        return response()->json(['success' => true], 200);
    }

    /**
     * удаление призов
     */
    public function deletePrize(Request $request, $id)
    {
        $validation = Validator::make(['id' => $id], [
            'id' => 'required|integer',
        ]);
        if ($validation->fails()) {

            $response = ['success' => false, 'errors' => $validation->getMessageBag()->toArray()];
            return response()->json($response, 422);

        } else {
            if (Prizes::where('id', $id)->delete()) {
                return response()->json(['success' => true], 200);
            } else {
                return response()->json(['success' => false], 422);
            }
        }
    }

    /**
     * редактирование призов
     */
    public function editPrize(Request $request, $id)
    {
        return Prizes::where('id', $id)->first();
    }

    /**
     * обновление призов
     */
    public function updatePrize(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'cost' => 'required|integer',
            'count' => 'required|integer',
        ],
            [
                'required' => 'поле :attribute не заполнено'
            ]);

        if ($validation->fails()) {
            $response = ['success' => false, 'errors' => $validation->getMessageBag()->toArray()];
            return response()->json($response, 422);

        } else {
            $prize = Prizes::find($id);
            $prize->name = $request->input('name');
            $prize->cost = $request->input('cost');
            $prize->count = $request->input('count');

            if ($prize->save()) {
                return response()->json(['success' => true], 200);
            } else {
                return response()->json(['success' => false], 422);
            }
        }
    }

    /*************************************************************************
     * ************************** Exel *****************************
     *************************************************************************/

    /**
     * вьюха для работы с Exel
     */
    public function importExportView()
    {
        return view('admin.import');
    }

    /**
     * импорт из exel в базу данных
     */
    public function import()
    {
        Excel::import(new ParticipantsImport, request()->file('file'), null, null);
        return back();
    }

    /**
     * создание хэшей для участников
     */
    public function makeLinks()
    {
        $participants = Participants::all();
        foreach ($participants as $participant) {
            if (isset($participant->id)) {
                for ($i = 1; $i <= 6; $i++) {
                    try {
                        $slugs = hash('sha256', random_int(1, 10000));
                    } catch (\Exception $e) {
                    }

                    $links = Links::where('slug', $slugs);
                    if ($links) {
                        $slugs = hash('sha256', random_int(10000, 100000));
                    }
                    $link = URL::to('/') . '/game/' . $slugs;

                    $param = [
                        'links' => $link,
                        'slug' => $slugs,
                    ];
                    $participant->links()->create($param);
                }
            }
        }
        return back();
    }

    /**
     * Экспорт мобильников и ссылок для них из базы в Exel
     */
    public function export()
    {
        return (new ParticipantsExports)->download('participants.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

//    public function queueImport()
//    {
//        Queue::push('App\Http\Controllers\AdminController@import');
//        return back();
//    }

//    public function queueMakeLinks()
//    {
//        Queue::push('App\Http\Controllers\AdminController@makeLinks');
//    }

//    public function queueExport()
//    {
//        Queue::push('App\Http\Controllers\AdminController@export');
//        return back();
//    }

    /*************************************************************************
     * ************************** Пользовательская часть **********************
     *************************************************************************/

    /**
     * вьюха игры с валидацией
     */
    public function showGame($slug)
    {
//        Redis::connection();
//        $link = Redis::get('links:slug:' . $slug);

        $links = Links::with('participants')->where('slug', $slug)->first();
        if (!empty($links)) {
            if ($links->status == 1) {
                return view('uPlayTheGame');
            }
        } else {
            return view('errorLink');
        }
        return view('index',
            ['slug' => $slug]);
    }

//    public function gameQueue($slug)
//    {
//        Queue::push('App\Http\Controllers\AdminController@game', $slug);
//    }

    /**
     * логика игры. Возвращает дату на ajax, чтобы выдать нужную картинку по id приза
     */
    public function game($slug, Request $request)
    {
        /*
        проверка из URL slug и slug из базы
        проверка сколько суммы у пользователя
        рандомное число от одного до 5
        если сумма пользователя меньше 2400
        подарок за 1000 рублей, если нету, то выдаем остальные в зависимости от остатка призового пулла
        если сумма больше 2400, но меньше 3400
        1, 2, 5-подарок за 300
        3-подарок за 200
        4-подарок за 100
        если сумма 3400
        1-5 - нет подарка
        */
        if ($request->ajax()) {
            $links = Links::with('participants')->where('slug', $slug)->first();
            $linkPartId = $links->participants_id;
            $linkStatus = $links->status;

            $prizePool = PrizePool::get();
            foreach ($prizePool as $pool) {
                $pool = $pool->prize_pool;
            }

            $participant = Participants::with('links')->where('id', $linkPartId)->get();
            foreach ($participant as $person) {
                $personSum = $person->sum;
            }

            $random = random_int(1, 5);
            if ($personSum < 2400) {
                switch ($random) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        $prize = Prizes::with('links')
                            ->where([
                                ['cost', '=', 1000],
                                ['count', '!=', 0]
                            ])->inRandomOrder()
                            ->first();
                        if ($prize) {
                            Participants::with('links')->where('id', $linkPartId)->increment('sum', 1000);
                            Prizes::with('links')->where('id', $prize->id)->decrement('count', 1);
                            $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                        } else {
                            if ($pool >= 300) {
                                $prize = Prizes::with('links')
                                    ->where([
                                        ['cost', '=', 300],
                                        ['count', '!=', 0]
                                    ])->first();
                                Participants::with('links')->where('id', $linkPartId)->increment('sum', 300);
                                $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                                PrizePool::decrement('prize_pool', 300);
                            } elseif ($pool >= 200) {
                                $prize = Prizes::with('links')
                                    ->where([
                                        ['cost', '=', 200],
                                        ['count', '!=', 0]
                                    ])->first();
                                Participants::with('links')->where('id', $linkPartId)->increment('sum', 200);
                                $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                                PrizePool::decrement('prize_pool', 200);
                            } elseif ($pool >= 100) {
                                $prize = Prizes::with('links')
                                    ->where([
                                        ['cost', '=', 100],
                                        ['count', '!=', 0]
                                    ])->first();
                                Participants::with('links')->where('id', $linkPartId)->increment('sum', 100);
                                $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                                PrizePool::decrement('prize_pool', 100);
                            } else {
                                $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => null]);
                            }
                        }
                        break;
                }
            } elseif ($personSum > 2400 and $personSum < 3400) {
                switch ($random) {
                    case 1:
                    case 2:
                    case 5:
                        if ($pool >= 300) {
                            $prize = Prizes::with('links')
                                ->where([
                                    ['cost', '=', 300],
                                    ['count', '!=', 0]
                                ])->first();
                            Participants::with('links')->where('id', $linkPartId)->increment('sum', 300);
                            $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                            PrizePool::decrement('prize_pool', 300);
                        } else {
                            $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => null]);
                        }
                        break;
                    case 3:
                        if ($pool >= 200) {
                            $prize = Prizes::with('links')
                                ->where([
                                    ['cost', '=', 200],
                                    ['count', '!=', 0]
                                ])->first();
                            Participants::with('links')->where('id', $linkPartId)->increment('sum', 200);
                            $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                            PrizePool::decrement('prize_pool', 200);
                        } else {
                            $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => null]);
                        }
                        break;
                    case 4:
                        if ($pool >= 100) {
                            $prize = Prizes::with('links')
                                ->where([
                                    ['cost', '=', 100],
                                    ['count', '!=', 0]
                                ])->first();
                            Participants::with('links')->where('id', $linkPartId)->increment('sum', 100);
                            $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                            PrizePool::decrement('prize_pool', 100);
                        } else {
                            $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => null]);
                        }
                        break;
                }
            } elseif ($personSum >= 3400) {
                switch ($random) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => null]);
                        break;
                }
            }

            Links::where('slug', $slug)->update(['status' => 1]);
            $links = Links::with('participants')->where('slug', $slug)->first();
            return $links;
        }

    }

    /**
     * если кто-то пытается уйти с showGame на URL '/'
     */
    public function showMainView()
    {
        return view('glavnaya');
    }
}
