<?php

namespace App\Http\Controllers;

use App\Admin;
use App\ResetPassword;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    /**
     *форма по отправке линка на почту существующего юзера
     */
    public function showForm()
    {
        return view('email.resetEmailForm');
    }

    /**
     * заполнение таблицы password reset и отправка письма
     */
    public function validatePasswordRequest(Request $request)
    {
        $validation = Validator::make($request->all(),
            [
                'email' => 'required|string|max:255|email|regex:([\w.-]+@([\w-]+)\.+\w{2,})'
            ],
            [
                'required' => 'поле :attribute не заполнено',
                'email.email' => ':attribute заполнен не верно',
            ]);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput($request->only('email'));
        } else {

            $admin = Admin::where('email', $request->email)->first();
            if (!isset($admin)) {
                return redirect()->back()->withErrors(['email' => trans('Такого пользователя не существует')]);
            }

            try {
                $randonInt = hash('sha256', random_int(1, 10000));
            } catch (\Exception $e) {
            }
            $param = [
                'email' => $request->email,
                'token' => $randonInt,
            ];

            ResetPassword::creatRessetPassword($param);

            $token = ResetPassword::where('email', $request->email)->first();

            $this->sendResetEmail($request->email, $token->token);
            return redirect()->back()->withErrors(['email' => trans('Вам на почту было выслано письмо с ссылкой')]);
        }
    }

    /**
     *отправка линка в письме для изменения пароля
     */
    private function sendResetEmail($email, $token)
    {
        $admin = Admin::where('email', $email)->select('name', 'email')->first();
        $url = URL::to('/');
        $link = URL::to('/') . '/resetPasswordForm/' . $token;

        $data = [
            'email' => $email,
            'link' => $link,
        ];

        Mail::send('email.resetPasswordEmail', $data, function ($message) use ($data) {
            $message->to($data['email'])->subject('Изменение пароля');
            $message->from(env('MAIL_USERNAME'), env('MAIL_USERNAME'));
        });
    }

    /**
     *валидация токена и форма для нового пароля
     */
    public function showResetForm($token)
    {
        //** валидация токена */
        if (isset($token)) {
            $tokenData = ResetPassword::where('token', $token)->first();
            if (!empty($tokenData)) {
                if ($tokenData->created_at < (Carbon::now()->subMinute(30))) {
                    ResetPassword::deleteRancidRows();
                    return redirect()->back()->withErrors(['email' => trans('Устаревший токен. Повторите отправку сообщения на mail')]);
                }
            } else {
                return redirect()->back();
            }
        }

        return view('email.reset', [
            'token' => $token,
            'email' => $tokenData['email'],
        ]);
    }

    /**
     *обновление нового пароля, удаление строки из таблицы password reset и отправка письма
     */
    public function resetPassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'password_confirmation' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->only(['password', 'password_confirmation']));
        }

        if ($request->password != $request->password_confirmation) {
            return redirect()->back()->withErrors(['password_confirmation' => 'Пароли не совпадают']);
        }

        $password = $request->password;
        $tokenData = ResetPassword::where('token', $request->token)->first();
        $admin = Admin::where('email', $tokenData->email)->first();
        if (!$admin) {
            return redirect()->back()->withErrors(['email' => 'Email not found']);
        }

        $admin->password = Hash::make($password);
        $admin->update();


        ResetPassword::where('email', $admin->email)->delete();

        $url = URL::to('/');
        $data =
            [
                'email' => $admin->email,
                'password' => $request->password,
                'url' => $url,
            ];

        JobController::sendMailAdmin($data);
        return view('admin.login');
    }


}
