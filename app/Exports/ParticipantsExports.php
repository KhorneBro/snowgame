<?php

namespace App\Exports;

use App\Participants;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ParticipantsExports implements FromCollection, ShouldAutoSize, ShouldQueue
{
    use Exportable, SerializesModels, Queueable;


    /**
     * @inheritDoc
     */
    public function collection()
    {
        $arr = collect();
        $participants = Participants::with('links')->get();
        foreach ($participants as &$participant) {
            foreach ($participant->links as $link) {
                $arr->push(collect([
                    $participant->mobile_number,
                    $link->links
                ]));
            }
        }
        return $arr;
    }

}
