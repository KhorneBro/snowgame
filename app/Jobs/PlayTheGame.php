<?php

namespace App\Jobs;

use App\Links;
use App\Participants;
use App\PrizePool;
use App\Prizes;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PlayTheGame implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @param $slug
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     * @throws \Exception
     */
    public function handle($slug)
    {
        $links = Links::with('participants')->where('slug', $slug)->first();
        $linkPartId = $links->participants_id;
        $linkStatus = $links->status;

        $prizePool = PrizePool::get();
        foreach ($prizePool as $pool) {
            $pool = $pool->prize_pool;
        }

        $participant = Participants::with('links')->where('id', $linkPartId)->get();
        foreach ($participant as $person) {
            $personSum = $person->sum;
        }


        $random = random_int(1, 5);
        if ($personSum < 2400) {
            switch ($random) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    $prize = Prizes::with('links')
                        ->where([
                            ['cost', '=', 1000],
                            ['count', '!=', 0]
                        ])->inRandomOrder()
                        ->first();
                    if ($prize) {
                        Participants::with('links')->where('id', $linkPartId)->increment('sum', 1000);
                        Prizes::with('links')->where('id', $prize->id)->decrement('count', 1);
                        $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                    } else {
                        if ($pool >= 300) {
                            $prize = Prizes::with('links')
                                ->where([
                                    ['cost', '=', 300],
                                    ['count', '!=', 0]
                                ])->first();
                            Participants::with('links')->where('id', $linkPartId)->increment('sum', 300);
                            $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                            PrizePool::decrement('prize_pool', 300);
                        } elseif ($pool >= 200) {
                            $prize = Prizes::with('links')
                                ->where([
                                    ['cost', '=', 200],
                                    ['count', '!=', 0]
                                ])->first();
                            Participants::with('links')->where('id', $linkPartId)->increment('sum', 200);
                            $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                            PrizePool::decrement('prize_pool', 200);
                        } elseif ($pool >= 100) {
                            $prize = Prizes::with('links')
                                ->where([
                                    ['cost', '=', 100],
                                    ['count', '!=', 0]
                                ])->first();
                            Participants::with('links')->where('id', $linkPartId)->increment('sum', 100);
                            $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                            PrizePool::decrement('prize_pool', 100);
                        } else {
                            $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => null]);
                        }
                    }
                    break;
            }
        } elseif ($personSum > 2400 and $personSum < 3400) {
            switch ($random) {
                case 1:
                case 2:
                case 5:
                    if ($pool >= 300) {
                        $prize = Prizes::with('links')
                            ->where([
                                ['cost', '=', 300],
                                ['count', '!=', 0]
                            ])->first();
                        Participants::with('links')->where('id', $linkPartId)->increment('sum', 300);
                        $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                        PrizePool::decrement('prize_pool', 300);
                    } else {
                        $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => null]);
                    }
                    break;
                case 3:
                    if ($pool >= 200) {
                        $prize = Prizes::with('links')
                            ->where([
                                ['cost', '=', 200],
                                ['count', '!=', 0]
                            ])->first();
                        Participants::with('links')->where('id', $linkPartId)->increment('sum', 200);
                        $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                        PrizePool::decrement('prize_pool', 200);
                    } else {
                        $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => null]);
                    }
                    break;
                case 4:
                    if ($pool >= 100) {
                        $prize = Prizes::with('links')
                            ->where([
                                ['cost', '=', 100],
                                ['count', '!=', 0]
                            ])->first();
                        Participants::with('links')->where('id', $linkPartId)->increment('sum', 100);
                        $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => $prize->id]);
                        PrizePool::decrement('prize_pool', 100);
                    } else {
                        $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => null]);
                    }
                    break;
            }
        } elseif ($personSum >= 3400) {
            switch ($random) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    $forReturn = Links::with('participants')->where('slug', $slug)->update(['prizes_id' => null]);
                    break;
            }
        }


        Links::where('slug', $slug)->update(['status' => 1]);
        $links = Links::with('participants')->where('slug', $slug)->first();
        return $links;
    }
}
