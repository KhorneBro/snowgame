<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Links extends Model
{
    public $incrementing = true;
    public $timestamps = true;
    protected $table = 'links';

    protected $fillable = [
        'links', 'slug', 'status', 'prizes_id'
    ];

    public function participants()
    {
        return $this->belongsTo('App\Participants');
    }

    public function prizes()
    {
        return $this->hasOne('App\Prizes');
    }

}
