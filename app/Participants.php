<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participants extends Model
{
    public $incrementing = true;
    public $timestamps = true;
    protected $table = 'participants';

    protected $fillable = [
        'mobile_number', 'sum'
    ];

    public function links()
    {
        return $this->hasMany('App\Links');
    }
}
