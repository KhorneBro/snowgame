function PlayTheGame(slug) {
    PostData('/game/' + slug, null);

}

function PostData(url, data, callback, callback_error) {
    $.ajax({
        url: url,
        dataType: 'JSON',
        data: data,
        type: 'GET',

        success: function (data) {
            console.log(data);
            jQuery(document).ready(function ($) {

                $('.first-sect').addClass('hide');
                $('.second-sect').removeClass('hide');
                //create a new instance of shake.js.
                var myShakeEvent = new Shake({
                    threshold: 15
                });

                // start listening to device motion
                myShakeEvent.start();

                // register a shake event
                window.addEventListener('shake', shakeEventDidOccur, false);

                //shake event callback
                function shakeEventDidOccur() {

                    //put your own code here etc.
                    $('.snowman__image').attr('src', 'participant/img/snowman3.png');
                    $('.snowman__image').css('cursor', 'default');
                    $('.snowman__image').effect('shake', {times: 15}, 4000);
                    $('.snowman__title').addClass('hide');
                    $('.snowball--first').addClass('show');
                    setTimeout(function () {
                        $('.snowball--second').addClass('show');
                    }, 1000);
                    setTimeout(function () {
                        $('.snowball--third').addClass('show');
                    }, 2000);
                    setTimeout(function () {
                        $('.snowball--fourth').addClass('show');
                    }, 3000);
                    setTimeout(function () {
                        $('.snowman__image').attr('src', 'participant/img/snowman4.png');
                        $('.snowman__title').text('Выбирай!');
                        $('.snowman__title').removeClass('hide');
                    }, 4000);
                }


                $('.snowman__image').one('click', function (e) {
                    $(this).attr('src', 'http://snowgame/participant/img/snowman3.png');
                    $('.snowman__image').css('cursor', 'default');
                    $(this).effect('shake', {times: 15}, 4000);
                    $('.snowman__title').addClass('hide');
                    $('.snowball--first').addClass('show');
                    setTimeout(function () {
                        $('.snowball--second').addClass('show');
                    }, 1000);
                    setTimeout(function () {
                        $('.snowball--third').addClass('show');
                    }, 2000);
                    setTimeout(function () {
                        $('.snowball--fourth').addClass('show');
                    }, 3000);
                    setTimeout(function () {
                        $('.snowman__title').text('Выбирай!');
                        $('.snowman__title').removeClass('hide');
                        $('.snowman__image').attr('src', 'http://snowgame/participant/img/snowman4.png');
                    }, 4000);
                });

                $('.snowball--win').click(function (e) {
                    console.log(data.prizes_id)
                    e.preventDefault();
                    $(this).find('img').attr('src', 'http://snowgame/participant/img/snowball-prize.png');
                    switch (data.prizes_id) {
                        case null:
                            $(this).find('img').attr('src', 'http://snowgame/participant/img/snowball-empty.png');
                            $('.snowman__image').attr('src', 'http://snowgame/participant/img/snowman1.png');
                            $('.snowman__title').text('Попробуй ещё раз!');
                            break;
                        case 1:
                            $('.snowman__image').attr('src', 'http://snowgame/participant/img/prize/Sertif__Sertif_snowman_KINOKINO.png');
                            $('.snowman__title').text('Поздравляю!');
                            break;
                        case 2:
                            $('.snowman__image').attr('src', 'http://snowgame/participant/img/prize/Sertif__Sertif_snowman_GETtttttttt.png');
                            $('.snowman__title').text('Поздравляю!');
                            break;
                        case 3:
                            $('.snowman__image').attr('src', 'http://snowgame/participant/img/prize/Sertif__Sertif_snowman_Magnit.png');
                            $('.snowman__title').text('Поздравляю!');
                            break;
                        case 4:
                            $('.snowman__image').attr('src', 'http://snowgame/participant/img/prize/Sertif__Sertif_snowman_TELEFON_100.png');
                            $('.snowman__title').text('Поздравляю!');
                            break;
                        case  6:
                            $('.snowman__image').attr('src', 'http://snowgame/participant/img/prize/Sertif__Sertif_snowman_TELEFON_ 200.png');
                            $('.snowman__title').text('Поздравляю!');
                            break;
                        case  8:
                            $('.snowman__image').attr('src', 'http://snowgame/participant/img/prize/Sertif__Sertif_snowman_TELEFON_ 300.png');
                            $('.snowman__title').text('Поздравляю!');
                            break;
                        case 9:
                            $('.snowman__image').attr('src', 'http://snowgame/participant/img/prize/Sertif__Sertif_snowman_кружка.png');
                            $('.snowman__title').text('Поздравляю!');
                            break;
                        case 10:
                            $('.snowman__image').attr('src', 'http://snowgame/participant/img/prize/Sertif__Sertif_snowman_Perekrestok.png');
                            $('.snowman__title').text('Поздравляю!');
                            break;
                    }
                    $('.prize-btn').addClass('show');
                });

                $('.snowball--lose').click(function (e) {
                    e.preventDefault();
                    $(this).find('img').attr('src', 'http://snowgame/participant/img/snowball-empty.png');
                    $('.snowman__image').attr('src', 'http://snowgame/participant/img/snowman1.png');
                    $('.snowman__title').text('Попробуй ещё раз!');
                });

                $('.prize-btn').click(function () {
                    $('.prize-btn').removeClass('show');
                    $('.prize').show();
                    $('html,body').animate({
                        scrollTop: $("#prize").offset().top
                    });
                });

            });

        },

        error: function (data) {
            if (data.status == 422) {
                if (callback_error != null) {
                    callback_error.call(window, data.responseJSON);
                }

            }

            if (data.status == 500) {
                // reload();
            }
        }

    });

    return false;
}
