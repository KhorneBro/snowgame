var prefix = 'admin/prizes';
var modal_edit = 'editPrizeModal';
var title_new = 'Добавить проект';

function editPrizeModal(data) {
    var form = $('#' + modal_edit + ' form');

    var title = 'Редактирование - ID:' + data.id;
    $('#' + modal_edit + ' .modal-title').html(title);
    $('input[name="name"]').val(data.name);
    $('input[name="cost"]').val(data.cost);
    $('input[name="count"]').val(data.count);
    $('#' + modal_edit + ' .modal-footer button[status="yes"]').attr('onclick', 'updatePrize("' + data.id + '");');
    $('#' + modal_edit).modal("show");
}


function editPrize(id) {
    loadEditData(prefix, id, editPrizeModal);
}

function updatePrize(id) {
    updateData(prefix, id, closeEditPrize);
}

function deletePrize(id) {
    ConfirmModal('danger', 'Внимание', 'Вы действительно хотите удалить проект со всей информацией о нем?', 'deleteData("' + prefix + '", "' + id + '", closeEditPrize);')
}

function defaultModal() {
    clearModal(prefix, modal_edit, title_new);
}

function closeEditPrize() {
    reload();
}



