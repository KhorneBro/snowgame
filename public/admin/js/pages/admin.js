var prefix = 'admin/administrators';
var modal_edit = 'editAdminModal';
var title_new = 'Добавить администратора';

function editAdminModal(data) {

    var form = $('#' + modal_edit + ' form');

    var title = 'Редактирование  : ' + data.name;
    $('#' + modal_edit + ' .modal-title').html(title);

    $('input[name="name"]').val(data.name);
    $('input[name="email"]').val(data.email);

    $('#' + modal_edit + ' .modal-footer button[status="yes"]').attr('onclick', 'updateAdmin("' + data.id + '");');
    $('#' + modal_edit).modal("show");
}


function editAdmin(id) {
    loadEditData(prefix, id, editAdminModal);
}

function updateAdmin(id) {
    updateData(prefix, id, closeEditAdmin);
}

function deleteAdmin(id) {
    ConfirmModal('danger', 'Внимание', 'Вы действительно хотите удалить администратора со всей информацией о нем?', 'deleteData("' + prefix + '", "' + id + '", closeEditAdmin);')
}


function defaultModal() {
    clearModal(prefix, modal_edit, title_new);
}

function closeEditAdmin() {
    reload();
}


