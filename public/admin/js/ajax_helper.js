$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

/**
 * Отправка данных методом POST
 * @param url  - url куда необходимо отправить запрос
 * @param data - json набор данных который нужно отправить {fName1:value1, fName2:value2, ...}.
 * @param callback - имя функции которую нужно вызвать после удачного ответа в нее придут полученные данные
 * @param callback_error - имя функции которую нужно вызвать после ошибки со статусом 422 в нее придут полученные данные
 * @returns {boolean}
 */
function postData(url, data, callback, callback_error) {
    $.ajax({
        url: url,
        dataType: 'JSON',
        data: data,
        type: 'POST',

        success: function (data) {
            if (callback != null) {
                callback.call(window, data);
            }
        },

        error: function (data) {
            if (data.status == 422) {
                if (callback_error != null) {
                    callback_error.call(window, data.responseJSON);
                }

            }

            if (data.status == 500) {
                // reload();
            }
        }
    });
    return false;
}

/**
 * Отправка данных методом GET
 * @param url  - url куда необходимо отправить запрос
 * @param data - json набор данных который нужно отправить {fName1:value1, fName2:value2, ...}.
 * @param callback - имя функции которую нужно вызвать после удачного ответа в нее придут полученные данные
 * @param callback_error - имя функции которую нужно вызвать после ошибки со статусом 422 в нее придут полученные данные
 * @returns {boolean}
 */
function getData(url, data, callback, callback_error) {
    $.ajax({
        url: url,
        dataType: 'JSON',
        data: data,
        type: 'GET',

        success: function (data) {
            if (callback != null) {
                callback.call(window, data);
            }
        },

        error: function (data) {
            if (data.status == 422) {
                if (callback_error != null) {
                    callback_error.call(window, data.responseJSON);
                }
            }

            if (data.status == 500) {
                // reload();
            }
        }
    });
    return false;
}

/**
 * Отправка данных методом DELETE
 * @param url  - url куда необходимо отправить запрос
 * @param data - json набор данных который нужно отправить {fName1:value1, fName2:value2, ...}.
 * @param callback - имя функции которую нужно вызвать после удачного ответа в нее придут полученные данные
 * @param callback_error - имя функции которую нужно вызвать после ошибки со статусом 422 в нее придут полученные данные
 * @returns {boolean}
 */
function postDeleteData(url, data, callback, callback_error) {
    $.ajax({
        url: url,
        dataType: 'JSON',
        data: data,
        type: 'DELETE',

        success: function (data) {
            if (callback != null) {
                callback.call(window, data);
            }
        },

        error: function (data) {
            if (data.status == 422) {
                if (callback_error != null) {
                    callback_error.call(window, data.responseJSON);
                }

            }

            if (data.status == 500) {
                reload();
            }
        }
    });
    return false;
}

/**
 * Отправка данных методом PATCH
 * @param url  - url куда необходимо отправить запрос
 * @param data - json набор данных который нужно отправить {fName1:value1, fName2:value2, ...}.
 * @param callback - имя функции которую нужно вызвать после удачного ответа в нее придут полученные данные
 * @param callback_error - имя функции которую нужно вызвать после ошибки со статусом 422 в нее придут полученные данные
 * @returns {boolean}
 */
function postPatchData(url, data, callback, callback_error) {
    $.ajax({
        url: url,
        dataType: 'JSON',
        data: data,
        type: 'PATCH',

        success: function (data) {
            if (callback != null) {
                callback.call(window, data);
            }
        },

        error: function (data) {
            if (data.status == 422) {
                if (callback_error != null) {
                    callback_error.call(window, data.responseJSON);
                }

            }

            if (data.status == 500) {
                reload();
            }
        }
    });
    return false;
}

/**
 * Отправка данных формы
 * @param url  - url куда необходимо отправить запрос
 * @param form - форма данные которой необходимо отправить
 * @param callback - имя функции которую нужно вызвать после удачного ответа в нее придут полученные данные
 * @param callback_error - имя функции которую нужно вызвать после ошибки со статусом 422 в нее придут полученные данные
 */
function sendForm(url, form, callback, callback_error) {
    var data = $(form).serialize();
    postData(url, data, callback, callback_error);
}

/**
 * Функция перезагрузки текущей страницы (для передечи в качестве callback функции)
 */
function reload() {
    console.log('reload');
    location.reload();
}


