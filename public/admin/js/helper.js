/**
 * Разбор параметров адресной строки
 */
$.extend({
    getUrlVars: function () {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    },
    getUrlVar: function (name) {
        return $.getUrlVars()[name];
    }
});
/**
 * Функция добавляет данные в GET-параметры
 * @param location
 */
function setLocation(location) {
    try {

        let parameters = '';
        let i = 0;
        let array = '';
        $.each(location, function (key, value) {
            if (location instanceof Array) {
                array = location.key.join(',');
                parameters += key + '=' + array + '&';
                array = '';
            } else {
                parameters += key + '=' + value + '&';
            }
        });
        parameters = parameters.substring(0, parameters.length - 1);
        history.pushState(null, null, '?' + parameters);
        return;
    } catch (e) {
    }
    location.hash = location;
}
/**
 * Аналог функции number_format языка php
 * @param number
 * @param decimals
 * @param decPoint
 * @param thousandsSep
 * @returns {string}
 */
function number_format(number, decimals, decPoint, thousandsSep) {
    decimals = decimals || 0;
    number = parseFloat(number);

    if (!decPoint || !thousandsSep) {
        decPoint = '.';
        thousandsSep = '';
    }

    var roundedNumber = Math.round(Math.abs(number) * ('1e' + decimals)) + '';
    // add zeros to decimalString if number of decimals indicates it
    roundedNumber = (1 > number && -1 < number && roundedNumber.length <= decimals)
        ? Array(decimals - roundedNumber.length + 1).join("0") + roundedNumber
        : roundedNumber;
    var numbersString = decimals ? roundedNumber.slice(0, decimals * -1) : roundedNumber.slice(0);
    var checknull = parseInt(numbersString) || 0;

    // check if the value is less than one to prepend a 0
    numbersString = (checknull == 0) ? "0" : numbersString;
    var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';

    var formattedNumber = "";
    while (numbersString.length > 3) {
        formattedNumber += thousandsSep + numbersString.slice(-3);
        numbersString = numbersString.slice(0, -3);
    }

    return (number < 0 ? '-' : '') + numbersString + formattedNumber + (decimalsString ? (decPoint + decimalsString) : '');
}

/**
 * Аналог функции Laravel str_limit - ограничивающая число символов в строке.
 * @param value
 * @param limit
 * @param end
 * @returns {*}
 */
function str_limit(value, limit, end){
    if (value.length <= limit){
        return value;
    }
    return value.substring(0,limit)+end;
}

/**
 * Генератор цветов
 * @returns {string}
 */
function getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

/**
 * генератор случайных цветов с контролем яркости
 * @param brightness 0-5 чем меньше цифра тем ярче
 * @returns {string}
 */
function getRandColor(brightness){

    // Six levels of brightness from 0 to 5, 0 being the darkest
    var rgb = [Math.random() * 256, Math.random() * 256, Math.random() * 256];
    var mix = [brightness*51, brightness*51, brightness*51]; //51 => 255/5
    var mixedrgb = [rgb[0] + mix[0], rgb[1] + mix[1], rgb[2] + mix[2]].map(function(x){ return Math.round(x/2.0)});
    return "rgb(" + mixedrgb.join(",") + ")";
}

/**
 * генератор случайного числа из диаппазона
 * @param from - от
 * @param to   - до
 * @returns {number}
 */
function getRandomRange(from,to)
{
    return Math.floor(Math.random()*(to-from+1)+from);
}

/**
 * Генератор GUID
 * @returns {string}  Пример: bdf5dd1c-7a1a-4aba-1399-2be2d4895f89
 */
function guid() {
    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    var result = (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();

    return result;
}

function apikey() {
    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    var result = (S4() + S4() + S4() + "4" + S4().substr(0, 3) + S4() + S4() + S4() + S4()).toLowerCase();

    return result;
}




