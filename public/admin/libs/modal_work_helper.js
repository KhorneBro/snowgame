/**
 * Создать данные
 * @param prefix - часть урла на который будут уходить запросы
 * @returns {boolean}
 */
function createData(prefix) {
    var id_modal = getIDFocusModal();
    $('#' + id_modal + ' .modal-footer button[status="yes"]').button('loading');
    $('#' + id_modal + ' .error-info').empty();
    var form = $('#' + id_modal + ' form');

    sendForm('/' + prefix + '/create', form, reload, errorShow);
    return false;
}
/**
 * Обновить данные
 * @param prefix - часть урла на который будут уходить запросы
 * @param id - код записи которую нужно изменить
 * @param callback - имя функции которую нужно вызвать после удачного ответа в нее придут полученные данные
 */
function updateData(prefix, id, callback) {
    var id_modal = getIDFocusModal();
    $('#' + id_modal + ' .modal-footer button[status="yes"]').button('loading');
    $('#' + id_modal + ' .error-info').empty();
    var form = $('#' + id_modal + ' form');


    sendForm('/' + prefix + '/update/' + id, form, callback, errorShow);
    return false;
}

/**
 * Удалить данные из базы
 * @param prefix - часть урла на который будут уходить запросы
 * @param id - код записи которую нужно удалить
 * @param callback - имя функции которую нужно вызвать после удачного ответа в нее придут полученные данные
 * @returns {boolean}
 */
function deleteData(prefix, id, callback) {
    postData('/' + prefix + '/delete/' + id, null, callback, reload);
    return false;
}

/**
 * Загрузка данных на редактирование
 * @param prefix - часть урла на который будут уходить запросы
 * @param id - код записи которую нужно отобразить
 * @param callback - функция для заполения модального окна полученными данными
 */
function loadEditData(prefix, id, callback) {
    postData('/' + prefix + '/edit/' + id, null, callback, reload);
    return false;
}

/**
 * Отображение ошибок валидации в модальном окне
 * @param data - json массив с ошибками
 */
function errorShow(data) {
    var id_modal = getIDFocusModal();

    var info = '<div class="bs-callout bs-callout-danger">' +
        '<h4>Обнаружены следующие ошибки</h4>' +
        '<ul class="ul-callout-danger">';

    $.each(data.errors, function (index, value) {
        info += '<li>' + value[0] + '</li>'
    });

    info += '</ul>' +
        '</div>';
    $('#' + id_modal + ' .error-info').html(info);
    $('#' + id_modal).animate({scrollTop: 0}, 'slow');
    $('#' + id_modal + ' .modal-footer button[status="yes"]').button('reset');
}

/**
 * Очистка полей формы модального окна и установка дефолтных значений
 * @param prefix
 * @param id_modal
 * @param title
 */
function clearModal(prefix, id_modal, title) {
    $('#' + id_modal + ' .error-info').empty();
    $('#' + id_modal + ' form')[0].reset();
    $('#' + id_modal + ' .modal-title').html(title);
    $('#' + id_modal + ' .modal-footer button[status="yes"]').attr('onclick', 'createData("' + prefix + '", "' + id_modal + '");');
    $('#' + id_modal + ' .modal-footer button[status="yes"]').button('reset');
}

/**
 * Получить id активного модального окна
 * @returns {string}
 */
function getIDFocusModal() {
    var id = '';
    $('.modal').each(function() {
        if ($(this).hasClass('in')){
          id = $(this).attr('id');
        }
    });
    return id;
}

/**
 * Заполнение модальной формы подтверждения
 * @param type_dialog - тип оформления окна (primary, info, warning, success, danger)
 * @param title - заголовок окна
 * @param message - сообщение в окне
 * @param confirm_function - название функции которая будет вызвана при нажатии на кнопку ОК
 * @constructor
 */
function ConfirmModal(type_dialog, title, message, confirm_function) {
    var modal = $('#confirm-modal');
    modal.removeClass('modal-primary');
    modal.removeClass('modal-info');
    modal.removeClass('modal-warning');
    modal.removeClass('modal-success');
    modal.removeClass('modal-danger');

    modal.addClass('modal-'+type_dialog);
    modal.find('.modal-title').text(title);
    modal.find('.modal-body p').text(message);
    modal.find('.modal-footer button[status="yes"]').attr("onClick", confirm_function);
}

/**
 * Возвращает фокус на предыдущее модальное окно
 */
var oldJqTrigger = jQuery.fn.trigger;
jQuery.fn.trigger = function()
{
    if ( arguments && arguments.length > 0) {
        if (typeof arguments[0] == "object") {
            if (typeof arguments[0].type == "string") {
                if (arguments[0].type == "show.bs.modal") {
                    var ret = oldJqTrigger.apply(this, arguments);
                    if ($('.modal:visible').length) {
                        $('.modal-backdrop.in').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) + 10);
                        $(this).css('z-index', parseInt($('.modal-backdrop.in').first().css('z-index')) + 10);
                    }
                    return ret;
                }
            }
        }
        else if (typeof arguments[0] == "string") {
            if (arguments[0] == "hidden.bs.modal") {
                if ($('.modal:visible').length) {
                    $('.modal-backdrop').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) - 10);
                    $('body').addClass('modal-open');
                }
            }
        }
    }
    return oldJqTrigger.apply(this, arguments);
};

