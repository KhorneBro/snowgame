/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

    config.baseFloatZIndex = 19000;
    config.startupFocus = true;
    //config.startupMode = 'source';

    config.language = 'ru';
    config.removeButtons = 'Underline,Subscript,Superscript';
    config.format_tags = 'p;h1;h2;h3;pre';
    config.removeDialogTabs = 'image:advanced;link:advanced;flash:advanced';

    config.removePlugins = 'elementspath,save,language,newpage,preview,templates,forms,blockquote,bidi,smiley,pagebreak,font';
    config.resize_enabled = false;
    config.allowedContent = true;

    config.extraPlugins = 'autogrow,tableresize,image2';

    config.filebrowserBrowseUrl = '/admin/libs/ckeditor/plugins/kcfinder/browse.php?type=files';
    config.filebrowserImageBrowseUrl = '/admin/libs/ckeditor/plugins/kcfinder/browse.php?type=images';
    config.filebrowserFlashBrowseUrl = '/admin/libs/ckeditor/plugins/kcfinder/browse.php?type=flash';
    config.filebrowserUploadUrl = '/2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e730/blog/upload';
    config.filebrowserImageUploadUrl = '/admin/libs/ckeditor/plugins/kcfinder/upload.php?type=images';
    config.filebrowserFlashUploadUrl = '/admin/libs/ckeditor/plugins/kcfinder/upload.php?type=flash';
};
