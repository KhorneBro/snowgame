<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'AdminController@showMainView');
Route::get('/showGame', 'AdminController@showMainView');
Route::get('/showGame/{slug}', 'AdminController@showGame');
Route::get('/game/{slug}', 'AdminController@game');

Route::post('/login', 'AdminLoginController@authenticated');
Route::get('/login_form', 'AdminLoginController@showForm')->name('admin.login');

Route::get('/resetEmailForm', 'ResetPasswordController@showForm');
Route::post('/password/reset/', 'ResetPasswordController@validatePasswordRequest');
Route::get('/resetPasswordForm/{token}', 'ResetPasswordController@showResetForm');
Route::post('/resetPassword', 'ResetPasswordController@resetPassword');

Route::middleware('admin')->group(function () {
    Route::prefix('admin')->group(function () {

        /*********
         * POST  *
         *********/
        Route::get('/logout', 'AdminLoginController@logout');
        /*********
         *  GET  *
         *********/
        Route::get('/', 'AdminController@index')->name('admin.dashboard');


        Route::prefix('administrators/')->group(function () {

            Route::get('/', 'AdminController@administrators');
            Route::post('create/', 'AdminController@addAdmin');
            Route::post('edit/{id}', 'AdminController@editAdmin')->where('id', '[0-9]+');
            Route::post('update/{id}', 'AdminController@updateAdmin')->where('id', '[0-9]+');
            Route::post('delete/{id}', 'AdminController@deleteAdmin')->where('id', '[0-9]+');
        });

        Route::prefix('prizes/')->group(function () {
            Route::get('/', 'AdminController@prizes');
            Route::post('create/', 'AdminController@addPrize');
            Route::post('delete/{id}', 'AdminController@deletePrize')->where('id', '[0-9]+');
            Route::post('edit/{id}', 'AdminController@editPrize')->where('id', '[0-9]+');
            Route::post('update/{id}', 'AdminController@updatePrize')->where('id', '[0-9]+');
        });

        Route::prefix('exel/')->group(function () {
            Route::get('/', 'AdminController@importExportView');
            Route::get('export', 'AdminController@export')->name('export');
            Route::get('makeLinks', 'AdminController@makeLinks')->name('makeLinks');
            Route::post('import', 'AdminController@import')->name('import');
        });
    });

});



