<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('links');
            $table->string('slug')->default(0)->nullable();
            $table->integer('status')->default(0);
            $table->bigInteger('prizes_id')->unsigned()->nullable();
            $table->bigInteger('participants_id')->unsigned();
            $table->foreign('prizes_id')->references('id')->on('prizes')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->foreign('participants_id')->references('id')->on('participants')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
