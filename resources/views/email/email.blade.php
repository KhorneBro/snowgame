<div class="well">
    <h3>Ваш доступ к CRM HD: </h3>

    <strong>Логин - {{ $email }}</strong>
    <br>
    <strong>Пароль - {{ $password }}</strong>
    <br>
    <a href="{{ $url }}">Перейти в HD CRM</a>

</div>
