<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="{{asset('participant/img/favicon.png') }}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Продлите радость новогоднего шопинга!</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <meta name="csrf-param" content="_csrf">
    <meta name="csrf-token"
          content="FgV_uIw6wPkv4hcOz5CxExAcR2R8Yv6m8ay0PkxkkywnPBrzzlSlz3amOjz2pOhKKEopLDk4svGu6NZsYRP6Qw==">


    <link href="{{asset('participant/css/vendor.css')}}" rel="stylesheet">
    <link href="https://necolas.github.io/normalize.css/5.0.0/normalize.css" rel="stylesheet">
    <link href="{{asset('participant/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('participant/css/media.css')}}" rel="stylesheet">
</head>
<body>

<main class="main">
    <header class="header">
        <div class="header__logo">
            <a href="#" target="_blank"><img src="{{asset('participant/img/logo-mastercard.svg') }}"
                                             alt=""></a>
            <a href="#" target="_blank"><img src="{{asset('participant/img/logo-alfa.svg') }}" alt=""></a>
        </div>
    </header>

    <p class="my-shake">shake</p>
    <section class="first-sect section">

        <div class="container">
            <h2 class="title">Потрясный снеговик!</h2>
            <img src="{{asset('participant/img/snowman1.png') }}" alt="" class="first-sect__image">
            <div class="first-sect__btn">
                <button id="play-btn" type="button" class="btn" onclick="PlayTheGame('{{$slug}}');">Играть!</button>
            </div>
        </div>

    </section>

    <section class="second-sect section hide">
        <div class="container">
            <div class="snowman">
                <h2 class="snowman__title">Потряси меня!</h2>
                <img src="{{asset('participant/img/snowman2.png') }}" alt="" class="snowman__image">
                <div class="snowman__balls">
                    <a href="#" class="snowball snowball--first snowball--win">
                        <img src="{{asset('participant/img/snowball.png') }}" alt="">
                    </a>
                    <a href="#" class="snowball snowball--second snowball--win">
                        <img src="{{asset('participant/img/snowball.png') }}" alt="">
                    </a>
                    <a href="#" class="snowball snowball--third snowball--win">
                        <img src="{{asset('participant/img/snowball.png') }}" alt="">
                    </a>
                    <a href="#" class="snowball snowball--fourth snowball--win">
                        <img src="{{asset('participant/img/snowball.png') }}" alt="">
                    </a>
                </div>
            </div>
            <button type="button" class="prize-btn">Как получить приз</button>
        </div>
    </section>

    <section id="prize" class="prize">
        <div class="container">
            <h2 class="prize__title">Всё очень просто!</h2>
            <!--                <p class="prize__subtitle">Всё, что Вам нужно это:</p>-->
            <p class="prize__text" style="text-align: center;">Информацию о получении приза, ожидайте в смс от банка до
                19.02.2020 года.</p>
            <img src="{{asset('participant/img/snowman6.png') }}" alt="" class="prize__image">
        </div>
    </section>

</main>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{asset('participant/js/shake.js') }}"></script>
<script src="{{asset('participant/js/jquery.inputmask.min.js') }}"></script>
<script src="{{asset('participant/js/main.js') }}"></script>
<script src="{{ asset('admin/js/ajax_helper.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/js/modal_work_helper.js') }}" charset="utf-8"></script>
<script src="{{ asset('admin/js/helper.js') }}"></script>
<script src="{{ asset('admin/js/main.js') }}"></script>

</body>
</html>
