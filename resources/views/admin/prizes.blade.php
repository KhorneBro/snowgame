@extends('admin.layouts.main')


@section('link_css')
@endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{$page_name}}
                <small>{{$unit_name}}</small>
            </h1>
        </section>

        <section class="content" style="min-height: 0px;">

            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-solid">

                        <div class="box box-info">
                            <div class="box-header">
                                <button type="submit" data-toggle="modal" data-target="#editPrizeModal"
                                        class="btn btn-primary ">Добавить
                                </button>
                            </div>
                            <div class="box-body" style="overflow: auto;">
                                <table class="table table-bordered">
                                    <thead>
                                    <th>Название</th>
                                    <th>Стоимость 1ого приза</th>
                                    <th>Общее количество призов</th>
                                    <th></th>
                                    <th></th>
                                    </thead>
                                    <tbody>
                                    @foreach(@$prizes as $item)
                                        <tr>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->cost}}</td>
                                            <td>{{$item->count}}</td>
                                            <td>
                                                <!-- Редактировать -->
                                                <button type="button" class="fa fa-pencil btn btn-default pull-right"
                                                        onclick="editPrize('{{$item->id}}');"></button>
                                            </td>
                                            <td>
                                                <!-- Удалить -->
                                                <button type="button" class="fa fa-times btn btn-default pull-right"
                                                        data-toggle="modal" data-target="#confirm-modal"
                                                        onclick="deletePrize('{{$item->id}}');"></button>
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </section>

    </div>
@endsection

@section('modal')
    @include('admin.modal_forms.edit_prizes')
    @include('admin.modal_forms.confirm')
@endsection

@section('link_js')
    <script src="{{asset('admin/js/pages/prizes.js')}}"></script>
@endsection

@section('document_ready')
    $("#"+modal_edit).on("hidden.bs.modal", function () {
    defaultModal();
    });

    defaultModal();
@endsection
