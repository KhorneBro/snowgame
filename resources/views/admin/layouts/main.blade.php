<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Snow Game</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/x-icon">

    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ asset('admin/libs/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('admin/libs/Font-Awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('admin/libs/ionicons/css/ionicons.min.css') }}">
    <!-- Select2 -->
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('admin/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('admin/css/skins/_all-skins.min.css') }}">
    <!-- iCheck -->

    <link rel="stylesheet" href="{{ asset('admin/css/helper.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/main.css') }}">



    @yield('link_css')
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">

<!-- Site wrapper -->
<div class="wrapper">
    @include('admin.layouts.top_menu')
    @include('admin.layouts.left_menu')

    @yield('content')

    @include('admin.layouts.footer')
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

@yield('modal')

<!-- jQuery 2.2.3 -->
<script src="{{ asset('admin/libs/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- jQueryUI -->
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('admin/libs/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('admin/libs/slimScroll/jquery.slimscroll.min.js') }}"></script>
<
<!-- AdminLTE App -->
<script src="{{ asset('admin/js/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('admin/js/demo.js') }}"></script>
<script src="{{ asset('admin/js/ajax_helper.js') }}"></script>

<script type="text/javascript" src="{{ asset('admin/js/modal_work_helper.js') }}" charset="utf-8"></script>

<script src="{{ asset('admin/js/helper.js') }}"></script>

<script src="{{ asset('admin/js/main.js') }}"></script>

@yield('link_js')

<script>
    $(document).ready(function () {
        @yield('document_ready')
    })
</script>

</body>
</html>
