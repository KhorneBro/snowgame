<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            {{--            @if(\Illuminate\Support\Facades\Auth::guard('users')->user())--}}
            <li class="treeview {!! Request::is('administrators') ? 'active' : null !!}">
                <a href="{{ url('admin/administrators') }}">
                    <i class="fa  fa-users"></i> <span>Администраторы</span>
                </a>
            </li>
            <li class="treeview {!! Request::is('prizes') ? 'active' : null !!}">
                <a href="{{ url('admin/prizes') }}">
                    <i class="fa  fa-files-o"></i> <span>Призы</span>
                </a>
            </li>
            <li>
                <a href="{{ url('admin/exel') }}">
                    <i class="fa  fa-asterisk"></i> <span>Exel</span>
                </a>
            </li>
        </ul>
    </section>


</aside>
