@extends('admin.layouts.main')
@php
    use App\User;
@endphp

@section('link_css')
@endsection

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>{{$page_name}}
                <small>{{$unit_name}}</small>
            </h1>
        </section>

        <section class="content" style="min-height: 0px;">

            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-solid">

                        <div class="box box-info">
                            <div class="box-header">
                                <button type="submit" data-toggle="modal" data-target="#editAdminModal"
                                        class="btn btn-primary ">Добавить
                                </button>
                            </div>
                            <div class="box-body" style="overflow: auto;">
                                <table class="table table-bordered">
                                    <thead>
                                    <th>Имя</th>
                                    <th>E-mail</th>
                                    <th></th>
                                    <th></th>
                                    </thead>
                                    <tbody>
                                    @foreach(@$administrators as $administrator)
                                        <tr>
                                            <td>{{$administrator->name}}</td>
                                            <td>{{$administrator->email}}</td>

                                            <td>
                                                <!-- Редактировать -->
                                                <button type="button" class="fa fa-pencil btn btn-default pull-right"
                                                        onclick="editAdmin('{{$administrator->id}}');"></button>
                                            </td>
                                            <td>
                                                <!-- Удалить -->
                                                <button type="button" class="fa fa-times btn btn-default pull-right"
                                                        data-toggle="modal" data-target="#confirm-modal"
                                                        onclick="deleteAdmin('{{$administrator->id}}');"></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('modal')
    @include('admin.modal_forms.edit_admin')
    @include('admin.modal_forms.confirm')
@endsection

@section('link_js')
    <script src="{{asset('admin/js/pages/admin.js')}}"></script>
@endsection

@section('document_ready')
    $("#"+modal_edit).on("hidden.bs.modal", function () {
    defaultModal();
    });

    defaultModal();
@endsection
