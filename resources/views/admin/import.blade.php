@extends('admin.layouts.main')
@php
    use App\Participants;
@endphp

@section('link_css')
@endsection

@section('content')
    <div class="content-wrapper">

        <section class="content" style="min-height: 0px;">
            <div class="row">
                <div class="box box-solid">
                    <div class="card-body">
                        <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="file" class="form-control">
                            <br>
                            <button class="btn btn-success">Импортировать данные</button>
                            <a class="btn btn-warning" href="{{ route('makeLinks') }}">Сделать ссылки</a>
                            <a class="btn btn-warning" href="{{ route('export') }}">Экспортировать данные</a>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

