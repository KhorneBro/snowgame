<div class="modal" id="confirm-modal" tabindex="-1" role="dialog" aria-labelledby="confirm-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Внимание</h4>
            </div>
            <div class="modal-body">
                <p>Вы действительно хотите удалить?</p>
            </div>
            <div class="modal-footer">
                <button type="button" status="no" class="btn btn-outline" data-dismiss="modal">Отмена</button>
                <button type="button" status="yes" class="btn btn-outline" data-dismiss="modal" onclick="">Ок</button>
            </div>
        </div>
    </div>
</div>