<div class="modal fade" id="editPrizeModal" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="error-info"></div>
                <form>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12">
                                <label for="name">Название:</label>
                                <input name="name" id="name" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12">
                                <label for="name">Стоимость 1ого приза:</label>
                                <input name="cost" id="cost" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12">
                                <label for="name">Общее количество призов:</label>
                                <input name="count" id="count" class="form-control">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" status="no" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary" status="yes" onclick="" id="load"
                        data-loading-text="<i class='fa fa-refresh fa-spin'></i>  Обработка">Сохранить
                </button>
            </div>
        </div>
    </div>
</div>
